-----------------------------------------------------------------------------------
-- Addon Name: LibEventAddonQueue
-- Creator: TaxTalis
-- Addon Ideal: Queue-based calling of addons on specific events
-- Addon Creation Date: November 28, 2020
--
-- File Name: LibEventAddonQueue.lua
-- File Description: This file contains the main definition of LibEventAddonQueue (LEA)
-- Load Order Requirements: First
-- 
-----------------------------------------------------------------------------------

LibEventAddonQueue = LibEventAddonQueue or {}
local LEA = LibEventAddonQueue

LEA.name = "LibEventAddonQueue"
LEA.author = "TaxTalis"

-- MAJOR.MINOR.PATCH
-- MAJOR version when addons integrating this library need to make changes
-- MINOR version when functionality in a backwards-compatible manner is modified, and
-- PATCH version when backwards-compatible bug fixes were made
LEA.addonVersion = "0.0.0"
LEA.addonQueue = {}

local function NewAddonQueue(event)
	local self = {}
	self.event = event
	self.queue = {}
	self.first = 0
	self.last = -1
	self.running = false
	self.currentEventArguments = {}
	self.executionIdentification = 0
	
	local function Pop(executionIdentification)
		if(self.executionIdentification ~= executionIdentification) then return end
		if(self.first <= self.last) then
			local addonName = self.queue[self.first].addonName
			local callback = self.queue[self.first].callback
			d("LEA Calling addon " .. addonName)
			self.first = self.first + 1
			callback(function() zo_callLater(function() Pop(self.executionIdentification) end, 1000) end, unpack(self.currentEventArguments))
		else
			self.running = false
		end
	end
	
	local function Run(...)
		self.executionIdentification = self.executionIdentification + 1
		self.currentEventArguments = {...}
		self.first = 0
		self.kast = -1
		
		local delay = 1
		if(running) then delay = 3000 end 
		zo_callLater(function() Pop(self.executionIdentification) end, delay)
	end
	
	local function Push(addonName, callback)
		self.last = self.last + 1
		self.queue[self.last] = {}
		self.queue[self.last].addonName = addonName
		self.queue[self.last].callback = callback
	end
	
	EVENT_MANAGER:RegisterForEvent(LEA.name, self.event, Run)
	
	return {Push = Push}
end

function LEA.RegisterForEvent(addonName, event, callback)
	if(LEA.addonQueue[event] == nil) then
		LEA.addonQueue[event] = NewAddonQueue(event)
	end
	zo_callLater(function() d("Addon " .. addonName .. " registered at LEA.") end, 3000)
	LEA.addonQueue[event].Push(addonName, callback)
end

local function OnAddOnLoaded(event, addonName)
  if (addonName == LEA.name) then
	EVENT_MANAGER:UnregisterForEvent(LEA.name, EVENT_ADD_ON_LOADED, OnAddOnLoaded)
  end
end
 
EVENT_MANAGER:RegisterForEvent(LEA.name, EVENT_ADD_ON_LOADED, OnAddOnLoaded)